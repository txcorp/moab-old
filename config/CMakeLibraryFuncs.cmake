#
# Functions and macros for creating shared libs

#
# Generate an export header that has a general define for the
# export header created by cmake.  Taken from the scimake project
#
# basedef:    The define that will lead to the directory definition
# incincfile: The name of the file to be generated
# dirdef:     The definition created when basedef is not defined
# dirincfile: The file to be included
#
macro(SciGenExportHeaderContainer basedef incincfile dirdef dirincfile)
  get_filename_component(def ${incincfile} NAME)
  string(TOUPPER "${def}" def)
  string(REGEX REPLACE "[\\.-]" "_" def "${def}")
  set(declinc
"
/**
 * Generated header, do not edit
 */
#ifndef ${def}
#define ${def}

#if !defined(${basedef}) || defined(__CUDA_ARCH__)
#define ${dirdef}
#endif
#include <${dirincfile}>

#endif // ${def}

"
  )
  file(WRITE ${incincfile} "${declinc}")
endmacro()

# Fix up a library, create appropriate defines for shared, static
#
# Proj:    The name of the project
# LibName: The name of the library with the capitalization it will have
macro(SciFixupLibrary Proj LibName DeclName)

  string(TOLOWER "${LibName}" libname)
  string(TOUPPER "${LibName}" LIBNAME)
  string(TOUPPER "${DeclName}" DECLNAME)

# Set the version
  if (BUILD_SHARED_LIBS)
    if (VERSION_MAJOR)
      set_target_properties(${LibName} PROPERTIES
          SOVERSION ${VERSION_MAJOR}.${VERSION_MINOR}
          VERSION ${VERSION_MAJOR}.${VERSION_MINOR}.${VERSION_PATCH}
      )
    endif ()
    message(STATUS "LibName = ${LibName}.")
    target_compile_definitions(${LibName}
      PRIVATE ${DeclName}_EXPORTS
      # ${Proj}_DLL # Added at the top
    )
  else ()
    target_compile_definitions(${LibName} PRIVATE ${DECLNAME}_STATIC_DEFINE)
  endif ()

  message(STATUS "Creating export headers for ${libname}.")
  generate_export_header(${LibName}
    EXPORT_FILE_NAME ${DeclName}Decl.h
    EXPORT_MACRO_NAME ${DECLNAME}_API
  )
  SciGenExportHeaderContainer(
    ${Proj}_DLL
    ${CMAKE_CURRENT_BINARY_DIR}/${DeclName}DeclInc.h
    ${DECLNAME}_STATIC_DEFINE
    ${DeclName}Decl.h
  )

endmacro()

